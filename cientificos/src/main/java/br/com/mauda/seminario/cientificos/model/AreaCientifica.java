package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class AreaCientifica {

    private Long id;
    private String nome;
    private List<Curso> cursos = new ArrayList<>();
    private List<Seminario> seminarios = new ArrayList<>();

    // Construtores
    public AreaCientifica() {
        super();
    }

    // Funções

    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }

    // Getter/Setters

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public void setSeminarios(List<Seminario> seminarios) {
        this.seminarios = seminarios;
    }

}
