package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Estudante estudante;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    // Construtores

    public Inscricao() {
        super();
    }

    public Inscricao(Seminario seminario) {
        this.dataCriacao = LocalDateTime.now();
        this.seminario = seminario;
        seminario.adicionarInscricao(this);

    }

    // Funções

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacao) && LocalDate.now().isBefore(this.seminario.getData())) {
            this.dataCompra = LocalDateTime.now();
            this.direitoMaterial = direitoMaterial;
            this.estudante = estudante;
            estudante.adicionaInscricao(this);
            this.situacao = SituacaoInscricaoEnum.COMPRADO;
        }
    }

    public void cancelarCompra() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao) && LocalDate.now().isBefore(this.seminario.getData())) {
            this.estudante.removeInscricao(this);
            this.estudante = null;
            this.dataCompra = null;
            this.direitoMaterial = null;
            this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        }
    }

    public void realizarCheckIn() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacao) && LocalDate.now().isBefore(this.seminario.getData())) {
            this.dataCheckIn = LocalDateTime.now();
            this.situacao = SituacaoInscricaoEnum.CHECKIN;
        }
    }

    // Getter/Setter

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

}
