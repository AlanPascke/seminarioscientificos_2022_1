package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL,
    COMPRADO,
    CHECKIN;

    private Long id;
    private String Nome;

    private SituacaoInscricaoEnum() {

    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.Nome;
    }

    public void setNome(String nome) {
        this.Nome = nome;
    }

}
