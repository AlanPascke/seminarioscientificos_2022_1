package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private boolean mesaRedonda;
    private LocalDate data;
    private Integer qtdInscricoes;
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    // Construtores

    public Seminario() {
        super();
    }

    public Seminario(AreaCientifica areasCientificas, Professor professor, Integer qtdInscricoes) {
        super();
        this.adicionarAreasCientificas(areasCientificas);
        this.adicionaProfessor(professor);
        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
        this.qtdInscricoes = qtdInscricoes;
    }

    // Funções

    public void adicionarAreasCientificas(AreaCientifica areasCientificas) {
        this.areasCientificas.add(areasCientificas);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionaProfessor(Professor professor) {
        this.professores.add(professor);
        professor.adicionarSeminario(this);
    }

    public boolean possuiareasCientificas(AreaCientifica areasCientificas) {
        return this.areasCientificas.contains(areasCientificas);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public boolean Professor(Professor professor) {
        return this.professores.contains(professor);
    }

    // Getters/Setter

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setqtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public void setAreasCientificas(List<AreaCientifica> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

}
